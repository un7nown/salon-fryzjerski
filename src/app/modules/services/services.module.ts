import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesComponent } from './components/services/services.component';
import { ServiceTileComponent } from './components/service-tile/service-tile.component';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from "../../shared/shared.module";
import { PricelistComponent } from './components/pricelist/pricelist.component';


@NgModule({
  declarations: [
    ServicesComponent,
    ServiceTileComponent,
    PricelistComponent
  ],
  imports: [
      SharedModule,
      MatCardModule,
    CommonModule,
  ]
})
export class ServicesModule { }
