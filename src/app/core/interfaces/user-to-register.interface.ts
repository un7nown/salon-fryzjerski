export interface UserToRegisterInterface {
    email: string,
    firstName: string,
    phoneNumber: number,
    password: string,
    gender: string,
}