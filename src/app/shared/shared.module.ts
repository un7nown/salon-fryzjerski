import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTabsModule} from "@angular/material/tabs";
import {MatDialogModule} from '@angular/material/dialog';
import { SnackbarComponent } from './components/snackbar/snackbar.component';

@NgModule({
  declarations: [
  
  
    SnackbarComponent
  ],
  imports: [
      MatDialogModule,
    MatCardModule,
    CommonModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatGridListModule,
    MatCheckboxModule,
    MatSelectModule,
    MatChipsModule,
    FontAwesomeModule,
    FlexLayoutModule,
      MatExpansionModule,
      MatTabsModule
  ],
  exports: [
      MatDialogModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSelectModule,
    MatChipsModule,
    FontAwesomeModule,
    FlexLayoutModule,
    MatExpansionModule,
      MatCardModule,
      MatTabsModule
  ]
})
export class SharedModule { }
