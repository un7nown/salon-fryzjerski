import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './components/gallery/gallery.component';
import { WorkerGalleryComponent } from './components/worker-gallery/worker-gallery.component';


@NgModule({
  declarations: [
    GalleryComponent,
    WorkerGalleryComponent,
  ],
  imports: [
    CommonModule,
  ]
})
export class GalleryModule { }
