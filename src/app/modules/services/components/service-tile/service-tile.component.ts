import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-service-tile',
  templateUrl: './service-tile.component.html',
  styleUrls: ['./service-tile.component.scss']
})
export class ServiceTileComponent implements OnInit {
  @Input() serviceName;

  constructor() {
  }

  ngOnInit(): void {
  }

}
