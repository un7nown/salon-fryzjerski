import { Injectable } from '@angular/core';
import {BehaviorSubject } from "rxjs";
import {Observable} from "rxjs";
import {currentDayOfWeek} from "../../helpers/opened-status";
import { translatedWeekdays } from "../../helpers/consts";
import dayjs from "dayjs";
import {DayInfoInterface} from "../interfaces/dayInfo-interface";


@Injectable({
  providedIn: 'root'
})


export class DatePickerService {
  dayInfo = new BehaviorSubject<DayInfoInterface>(null);

  constructor() { }

  setDayInfo(dayInfo: DayInfoInterface) {
    dayInfo.month
    this.dayInfo.next(dayInfo);
  }

  getWeekDayToString(dayInfo: DayInfoInterface) {

  }



  // setWeekdayName(dayName : number) : void {
  //   this.dayName.next(this.dayInfo.day)
  // }
}
