import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './components/contact/contact.component';
import { SharedModule } from "../../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ClipboardModule} from "@angular/cdk/clipboard";


@NgModule({
  declarations: [
    ContactComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ClipboardModule
    ]
})
export class ContactModule { }
