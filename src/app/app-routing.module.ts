import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { ContactComponent } from './modules/contact/components/contact/contact.component';
import { GalleryComponent } from './modules/gallery/components/gallery/gallery.component';
import { RegisterComponent } from './modules/account/components/register/register.component';
import { ReservationsComponent } from './modules/reservations/components/reservations/reservations.component';
import { ServicesComponent } from './modules/services/components/services/services.component';
import { LoginComponent } from './modules/account/components/login/login.component';
import { RemindMyPasswordComponent } from './modules/account/components/remind-my-password/remind-my-password.component';
import { WorkerPanelComponent } from "./modules/workers/components/worker-panel/worker-panel.component";

const routes: Routes = [
  { path: 'przypomnij-haslo', component: RemindMyPasswordComponent },
  { path: 'login', component: LoginComponent },
  { path: 'uslugi', component: ServicesComponent },
  { path: 'kontakt', component: ContactComponent },
  { path: 'galeria', component: GalleryComponent },
  { path: 'rejestracja', component: RegisterComponent },
  { path: 'rezerwacja', component: ReservationsComponent },
  { path: 'panel-pracownika', component: WorkerPanelComponent},
  { path: '', component: HomeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }, // redirect to `first-component`
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
