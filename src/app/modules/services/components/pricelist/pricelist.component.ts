import { Component, Input, OnInit} from '@angular/core';
import {ServicesDetailsInterface} from "../../../../core/interfaces/services-details.interface";
import {servicesDetails} from "../../../../shared/services-details";
import {Observable} from "rxjs";
import {BreakpointObserver} from "@angular/cdk/layout";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-pricelist',
  templateUrl: './pricelist.component.html',
  styleUrls: ['./pricelist.component.scss']
})

export class PricelistComponent implements OnInit {
  @Input() serviceDetail;
  hairServices : ServicesDetailsInterface[];
  panelOpenState = false;
  $isHandset: Observable<boolean> = this.breakpointObserver.observe('(min-width: 600px)')
      .pipe(
          map(result => result.matches),
      );

  constructor(private breakpointObserver: BreakpointObserver) {

  }

  ngOnInit(): void {
    this.hairServices = servicesDetails;
  }

}

