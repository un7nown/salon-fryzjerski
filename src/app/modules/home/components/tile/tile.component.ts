import { Component, OnInit } from '@angular/core';
import { ServicesDetailsInterface } from "../../../../core/interfaces/services-details.interface";
import { servicesDetails } from "../../../../shared/services-details";

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {
  hairServices : ServicesDetailsInterface[]
  constructor() {
    this.hairServices = servicesDetails;
  }

  ngOnInit(): void {
  }

}
