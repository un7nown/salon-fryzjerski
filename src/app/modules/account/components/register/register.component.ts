import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  genders = ['mężczyzna', 'kobieta'];
  signupForm: FormGroup;
  hide: boolean = true;

  ngOnInit() {

  }

  onSubmit() {
    console.log(this.signupForm.value);
  }

  constructor(private formBuilder : FormBuilder) {
    this.signupForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.email]),
      firstName: new FormControl(null, [Validators.required, Validators.pattern("[a-zA-zżźćńółęąśŻŹĆĄŚĘŁÓŃ ]*"), Validators.maxLength(16)]),
      password: new FormControl(null, [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,32}$")]),
      repeatPassword: new FormControl(null,[Validators.required]),
      phoneNumber: new FormControl(null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(9), Validators.maxLength(9)]),
      gender: new FormControl(null, Validators.required),
    })
  }
}

