import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutModule } from '@angular/cdk/layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RemindMyPasswordComponent } from './components/remind-my-password/remind-my-password.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';


@NgModule({
  declarations: [
    RegisterComponent,
    LoginComponent,
    RemindMyPasswordComponent,
    LoginDialogComponent,
  ],
  imports: [
    LayoutModule,
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCheckboxModule,
  ]
})
export class AccountModule { }
