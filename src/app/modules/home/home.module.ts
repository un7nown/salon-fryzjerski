import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from "@angular/router";
import { BannerComponent } from './components/banner/banner.component';
import { TileComponent } from './components/tile/tile.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';

@NgModule({
  declarations: [
    HomeComponent,
    BannerComponent,
    TileComponent,
    AboutUsComponent,
    CarouselComponent,
  ],
  imports: [
    IvyCarouselModule,
    CommonModule,
    SharedModule,
    RouterModule,
  ],
  exports: [
      BannerComponent
  ]
})
export class HomeModule {

}
