import { Component, OnInit } from '@angular/core';
import { servicesDetails } from "../../../../shared/services-details";
import {ServicesDetailsInterface} from "../../../../core/interfaces/services-details.interface";

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  images: Array<any> = [
    {
      path: 'assets/farbowanie-kobieta.svg',
      name: "Farbowanie damskie"
    }
  ];
  hairServices : ServicesDetailsInterface[];


  constructor() {
    this.hairServices = servicesDetails;
  }

  ngOnInit(): void {
  }


}
