import { Component, OnInit } from '@angular/core'; //aby wiedziec, ze to komponent

@Component({ //opisuje dane dotyczace komponentu
  selector: 'app-home', //np. do routingu
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit { //klasa
  tablica = ['xd', 2, 3, 4];
  siema = 'siema';
  xd = 'xd';
  constructor() { }

  ngOnInit(): void {
  }

}
