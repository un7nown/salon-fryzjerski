import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemindMyPasswordComponent } from './remind-my-password.component';

describe('RemindMyPasswordComponent', () => {
  let component: RemindMyPasswordComponent;
  let fixture: ComponentFixture<RemindMyPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemindMyPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemindMyPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
