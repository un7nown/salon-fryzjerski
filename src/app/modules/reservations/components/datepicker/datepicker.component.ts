import {Component, OnInit} from '@angular/core';
import dayjs from "dayjs";
import {translatedMonths, translatedWeekdays} from "../../../../helpers/consts";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { DatePickerService } from "../../../../core/services/date-picker.service"
import { isOpenedHourStatus } from "../../../../helpers/opened-status";

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
})

export class DatepickerComponent implements OnInit {
  faAngleLeft = faAngleLeft;
  faAngleRight = faAngleRight;
  monthTranslated: string = translatedMonths[dayjs().format('MMMM')];
  monthFirstDay : number;
  daysInMonth: number[] = [];
  translatedMonthsArray = Object.values(translatedMonths);
  monthIndex: number = this.translatedMonthsArray.indexOf(this.monthTranslated);
  showTimepicker : boolean = false;
  clickCount: number = 0;
  leftArrowClickState: boolean = true;
  rightArrowClickState: boolean = false;
  todayDayOfMonth: number = dayjs().date();
  selectedDayOfWeek: number;
  selectedDayOfMonth: number;
  selectedMonth: number;


  ngOnInit(): void {
    console.log(translatedMonths[3]);
    this.generateMonthArray(0);
  }


  constructor(private pickerService: DatePickerService) {

  }

  isOpenedToday = (item) =>  {
    console.log(1);
    return isOpenedHourStatus(item);
  }

  generateMonthArray(shiftOnArrows: number): void {
    let daysInCurrentMonth: number = dayjs().add(shiftOnArrows, 'month').daysInMonth();
    this.daysInMonth = [];
    for (let dayOfThisMonth: number = 1; dayOfThisMonth <= daysInCurrentMonth; dayOfThisMonth++) {
      this.daysInMonth[dayOfThisMonth-1] = Number(dayjs().date(dayOfThisMonth).format('D'));
    }
    this.daysInMonth = this.shiftStartDayMonth(shiftOnArrows).concat(this.daysInMonth);
  }

  shiftStartDayMonth(shift: number): Array<number> {
    this.monthFirstDay = dayjs().add(shift, 'month').startOf('M').get('d');
    return Array(this.getSunday());
  }

  getSunday(): number {
      if (this.monthFirstDay === 0) {
        return 6;
      } else return this.monthFirstDay-1;
  }

  showDaySchedule(dayOfMonth: number) : void {
    this.selectedDayOfMonth = dayOfMonth;
    this.selectedDayOfWeek = dayjs().date(dayOfMonth).get('day');
    this.selectedMonth = dayjs().date(dayOfMonth).get('month')+this.clickCount;
    const dayInfo = {
      day: dayOfMonth,
      weekday: translatedWeekdays[this.selectedDayOfWeek],
      weekdayNumber: this.selectedDayOfWeek,
      month: this.translatedMonthsArray[this.selectedMonth],
      monthNumber: this.selectedMonth,
      year: dayjs().year()
    };
    this.pickerService.setDayInfo(dayInfo);
    this.showTimepicker = true;
  }

  previousMonth() : number {
    let clickCounter = --this.clickCount;
    this.monthTranslated = this.translatedMonthsArray[this.monthIndex-clickCounter];
    this.generateMonthArray(clickCounter);
    this.rightArrowClickState = false;
    this.leftArrowClickState = true;
    return this.monthIndex;
  }

  nextMonth() : number {
    let clickCounter: number = ++this.clickCount;
    this.monthTranslated = this.translatedMonthsArray[this.monthIndex+clickCounter];
    this.generateMonthArray(clickCounter);
    this.leftArrowClickState = false;
    this.rightArrowClickState = true;
    return this.monthIndex+1;
  }

  isSunday(currentDay : number, currentMonth : number) : boolean {
    let dayUpdatedByItem = new Date(dayjs().year(), currentMonth, currentDay);
    return ((dayjs(dayUpdatedByItem).day()) === 0);
  }

  isEarlierThanToday(currentDay: number, currentMonth : number) : boolean {
    return (currentDay < this.todayDayOfMonth) && (currentMonth === this.monthIndex);
  }

  isTodayClosed() {
    //wez dzisiejszy dzien, sprawdz jego dzien tygodnia (numer 0-6), porownaj z godzinami. Jesli otwarty ->

  }


}
