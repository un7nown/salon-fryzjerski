export interface DayInfoInterface {
    day: number,
    weekday: string,
    weekdayNumber: any,
    month: string,
    monthNumber: any,
    year: number
}