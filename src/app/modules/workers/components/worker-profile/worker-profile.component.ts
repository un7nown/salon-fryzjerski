import { Component, OnInit } from '@angular/core';
import { workerData } from "../../../../shared/worker-data";
import {WorkerDataInterface} from "../../../../core/interfaces/worker-data.interface";

@Component({
  selector: 'app-worker-profile',
  templateUrl: './worker-profile.component.html',
  styleUrls: ['./worker-profile.component.scss']
})
export class WorkerProfileComponent implements OnInit {
  workerDetails : WorkerDataInterface[];
  constructor() {
    this.workerDetails=workerData;
  }

  ngOnInit(): void {
  }

}
