import { Component, OnInit } from '@angular/core';
import { faFacebook, } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faPhone, faCut  } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faCut = faCut;
  faFacebook = faFacebook;
  faEnvelope = faEnvelope;
  faPhone = faPhone;

  constructor() { }

  ngOnInit(): void {
  }
}
