import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerGalleryComponent } from './worker-gallery.component';

describe('WorkerGalleryComponent', () => {
  let component: WorkerGalleryComponent;
  let fixture: ComponentFixture<WorkerGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
