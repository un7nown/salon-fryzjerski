import dayjs from "dayjs";

export const currentHour: number = dayjs().get('h');
export const currentDayOfWeek: number = dayjs().get('day');

export function isOpenedHourStatus(dayOfWeek: number) : boolean {
    console.log(2);
    return currentHour >= 10 && currentHour <= 14 && dayOfWeek !== 0 || currentHour >= 10 && currentHour <= 19 && dayOfWeek !== 0 && dayOfWeek !== 6;
}