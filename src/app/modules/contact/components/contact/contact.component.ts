import { Component} from '@angular/core';
import { faEnvelope, faPhone, faAddressCard, faClock } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, } from "@fortawesome/free-brands-svg-icons";
import { isOpenedHourStatus, currentDayOfWeek } from "../../../../helpers/opened-status";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {
  faClock = faClock;
  faAddressCard = faAddressCard;
  faEnvelope = faEnvelope;
  faPhone = faPhone;
  faFacebook = faFacebook;
  isOpened = isOpenedHourStatus(currentDayOfWeek);

  constructor() { }

}
