import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './modules/home/home.module';
import { ContactModule } from './modules/contact/contact.module';
import { GalleryModule } from './modules/gallery/gallery.module';
import { AccountModule } from './modules/account/account.module'; //zmiana nazwy z RegisterModule na AccountModule!
import { ReservationsModule } from './modules/reservations/reservations.module';
import { ServicesModule } from './modules/services/services.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './core/navbar/navbar.component';
import { FooterComponent } from './core/footer/footer.component';
import { CommonModule } from '@angular/common';
import { EnrollmentsModule } from './modules/enrollments/enrollments.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WorkersModule} from "./modules/workers/workers.module";


//stworzyc modul angular materials i wrzucic tu do importu

@NgModule({
  declarations: [ //tu mowimy Anuglarowi, ze hej, stworzylem komponent
    AppComponent, NavbarComponent, FooterComponent,
  ],
  imports: [ //dodaje moduly, jesli jest ich wiecej
    CommonModule,
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HomeModule,
      WorkersModule,
    ContactModule,
    GalleryModule,
    EnrollmentsModule,
    AccountModule,
    ReservationsModule,
    ServicesModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    FlexLayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
