import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-remind-my-password',
  templateUrl: './remind-my-password.component.html',
  styleUrls: ['./remind-my-password.component.scss']
})
export class RemindMyPasswordComponent implements OnInit {
  remindForm: FormGroup;

  ngOnInit() {

  }

  onSubmit() {
    console.log('gowno');
  }


  constructor(private formBuilder : FormBuilder) {
    this.remindForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.email]),
      passcode: new FormControl(null, [Validators.minLength(6), Validators.maxLength(6)])
    })
  }
}
