import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-snackbar',
  template: `<span class="snackbar">Pizza party!!!{{text}}</span>`,
  styles: [`
    .snackbar {
      color: hotpink;
    }
  `],
})

export class SnackbarComponent implements OnInit {
@Input() text;
  constructor() { }

  ngOnInit(): void {
  }

}
