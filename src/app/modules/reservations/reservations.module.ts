import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { MatStepperModule } from '@angular/material/stepper';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { TimepickerComponent } from './components/timepicker/timepicker.component';
import { DatePickerService } from "../../core/services/date-picker.service";


@NgModule({
  declarations: [
    ReservationsComponent,
    DatepickerComponent,
    TimepickerComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    MatStepperModule,
    ReactiveFormsModule,
  ],
  providers: [ DatePickerService ],
  exports: [ TimepickerComponent, DatepickerComponent]
})
export class ReservationsModule {
}
