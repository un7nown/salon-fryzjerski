import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "../../shared/shared.module";
import { WorkerPanelComponent } from './components/worker-panel/worker-panel.component';
import { WorkerProfileComponent } from './components/worker-profile/worker-profile.component';
import { ReservationsModule } from "../reservations/reservations.module";
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';

@NgModule({
  declarations: [
    WorkerProfileComponent,
    WorkerPanelComponent,
    EditProfileComponent,
  ],
  imports: [
      ReservationsModule,
      SharedModule,
    CommonModule
  ]
})
export class WorkersModule { }
