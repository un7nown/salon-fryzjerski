import { Component, OnInit } from '@angular/core';
import {faAngleLeft, faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { generateHours } from "../../../../helpers/generate-hours";
import { DatePickerService } from "../../../../core/services/date-picker.service";
import {translatedWeekdays} from "../../../../helpers/consts";
import dayjs from "dayjs";
import {DayInfoInterface} from "../../../../core/interfaces/dayInfo-interface";

@Component({
  selector: 'app-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent implements OnInit {
  faAngleLeft = faAngleLeft;
  faAngleRight = faAngleRight;
  morningHours: any = generateHours(10, 13);
  afternoonHours: any = generateHours(13, 16);
  eveningHours: any = generateHours(16, 19);
  importedDayInfo: DayInfoInterface;
  clickCounter : number = 0;

  constructor(private pickerService: DatePickerService) { }

  ngOnInit(): void {
    this.pickerService.dayInfo.subscribe(dayInfo => {
      this.importedDayInfo=dayInfo;
    })
  }

  sendReservation() {
    //przygotuj paczke importedDayInfo, dodaj timestamp (godzina rezerwacji) i wyslij
  }

  onTimeSelect() {

  }

  previousDay() {
    let clickCount = --this.clickCounter
    this.pickerService.dayInfo.next(this.importedDayInfo.weekdayNumber);
  }

  nextDay() {
    let clickCount = ++this.clickCounter
  }




}
