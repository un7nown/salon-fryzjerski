export function generateHours(startHour: number, endHour: number){
    const quarterHour = ["00", "15", "30", "45"];
    const hours = [];
    for (let hour=startHour; hour < endHour; hour++) {
        for (let quarter = 0; quarter < 4; quarter++) {
             hours.push(hour+":"+quarterHour[quarter]);
        }
    }
    return hours;
}