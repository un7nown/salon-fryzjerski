export const servicesDetails = [
        {
            "serviceId": 0,
            "serviceName" : "Strzyżenie męskie",
            "isForMen": true,
            "servicePrice": 50,
            "serviceTime": 45,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort" : "Konsultacja, strzyżenie włosów nożyczkami lub maszynką.",
            "serviceDescription": "Fryzura jest wizytówką każdego mężczyzny. Bez względu na to czy Twoje włosy to bujne faliste loki, czy też proste rzadkie włosy, dopasujemy fryzurę w sam raz dla Ciebie."
        },
        {
            "serviceId": 1,
            "serviceName" : "Broda",
            "isForMen": true,
            "servicePrice": 30,
            "serviceTime": 30,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort" : "Konsultacja, pielęgnacja gorącym ręcznikiem, strzyżenie zarostu.",
            "serviceDescription": "Zarost potrafi diametralnie zmienić wygląd mężczyzny. Trzydniowy zarost? Broda drwala? W zależności od typu Twojego zarostu potrafimy dodać Twojej twarzy jeszcze więcej męskości."
        },
        {
            "serviceId": 2,
            "serviceName" : "Brzytwa",
            "isForMen": true,
            "servicePrice": 20,
            "serviceTime": 15,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort": "Konsultacja, strzyżenie zarostu brzytwą lub poprawa konturów.",
            "serviceDescription": "Twoja fryzura i broda jest perfekcyjna? Świetnie! Podkreślmy to konturami, które nakreślimy naszymi brzytwami. Konturujemy zarówno Twój zarost, jak i fryzurę."
        },
        {
            "serviceId": 3,
            "serviceName" : "Combo",
            "isForMen": true,
            "servicePrice": 90,
            "serviceTime": 90,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort": "Kompleksowa pielengnacja - strzyżenie + brzytwa + broda.",
            "serviceDescription": "Jeśli jesteś gotowy na prawdziwą metamorfozę, combo jest idealną opcją dla Ciebie! Odmienimy Twoją fryzurę, wypielęgnujemy i odświeżymy zarost, a na koniec nadamy konturu."
        },
        {
            "serviceId": 4,
            "serviceName" : "Strzyżenie i stylizacja",
            "isForMen": false,
            "servicePrice": 70,
            "serviceTime": 60,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort": "Konsultacja, stylizacja i strzyżenie włosów.",
            "serviceDescription": "Włosy to skarb, który należy pielęgnować. Zadbamy o to, abyś Twoją nową fryzurą w pełni wyraziła siebie. Bez względu na to czy chcesz przyciąć końcówki, czy grzywkę - ta usługa jest dla Ciebie."
        },
        {
            "serviceId": 5,
            "serviceName" : "Loki i fale",
            "isForMen": false,
            "servicePrice": 80,
            "serviceTime": 60,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort": "Konsultacja, lokowanie/falowanie włosów.",
            "serviceDescription": "Pragniesz swojemu życiu dodać odrobinę szaleństwa? Zacznij od fryzury! Sprawimy, że Twoje włosy zakręcą się w uporządkowany sposób."
        },
        {
            "serviceId": 6,
            "serviceName" : "Prostowanie",
            "isForMen": false,
            "servicePrice": 50,
            "serviceTime": 45,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort": "Konsultacja, prostowanie keratynowe włosów.",
            "serviceDescription": "Jeśli masz już dość nieuporządkowanych włosów, ta usługa jest dla Ciebie. Dzięki keratynie i umiejętnościom naszych specjalistów Twoje włosy osiągną upgragnioną gładkość i sprężystość."
        },
        {
            "serviceId": 7,
            "serviceName" : "Farbowanie",
            "isForMen": false,
            "servicePrice": 50,
            "serviceTime": 60,
            "pathToImage": "url(~src/assets/loki-kobieta.svg)",
            "serviceDescriptionShort": "Konsultacja, farbowanie włosów.",
            "serviceDescription": "Blond? Mahoń? A może... róz? Dodaj swoim włosom trochę koloru. Połączenie profesjonalnych farb oraz naszego doświadczenia spowoduje, że Twoim nowym kolorze będziesz jeszcze bardziej zjawiskowa."
        }
    ]