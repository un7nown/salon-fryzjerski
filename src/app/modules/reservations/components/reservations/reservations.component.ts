import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {BreakpointObserver} from "@angular/cdk/layout";
import {ServicesDetailsInterface} from "../../../../core/interfaces/services-details.interface";

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit {
  sexFormGroup: FormGroup;
  serviceFormGroup: FormGroup;
  calendarFormGroup: FormGroup;
  paymentFormGroup: FormGroup;
  maleStatus = true;
  maleServices: string[] = ['strzyżenie', 'broda', 'combo', 'brzytwa'];
  femaleServices  = ['farbowanie', 'prostowanie', 'loki', 'strzyżenie'];
  importedServicesInfo : ServicesDetailsInterface;


  $isHandset: Observable<boolean> = this.breakpointObserver.observe('(min-width: 715px)')
      .pipe(
          map(result => result.matches),
      );

  onToggleFemale() {
    this.maleStatus = false;
  }
  onToggleMale() {
    this.maleStatus = true;
  }


  constructor(private formBuilder: FormBuilder, private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
    this.sexFormGroup = this.formBuilder.group({
      sex: ['', Validators.required]
    });
    this.serviceFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.calendarFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.paymentFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
}
