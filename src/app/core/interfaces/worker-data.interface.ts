export interface WorkerDataInterface {
    id: number;
    email: string,
    firstName: string,
    lastName: string,
    description: string,
    phoneNumber: number,
    password: string,
    services: number[];
    role: string;
}