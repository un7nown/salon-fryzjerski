export interface ServicesDetailsInterface {
    serviceId: number;
    serviceName: string;
    isForMen: boolean;
    servicePrice: number;
    serviceTime: number;
    pathToImage: string;
    serviceDescriptionShort: string;
    serviceDescription: string;
}
